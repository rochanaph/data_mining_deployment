import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
# sumber bacaan flask ML 
# https://rahmadya.com/2020/06/11/flask-dan-jinja2-untuk-aplikasi-machine-learning-berbasis-web/
app = Flask(__name__)
modelRegresi = pickle.load(open("model.pkl", "rb"))

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/predict", methods=["POST"])
def predict():
    '''
    Untuk hasil render HTML GUI
    '''
    int_features = [int(x) for x in request.form.values()]
    final_features = [np.array(int_features)]
    prediction = modelRegresi.predict(final_features)

    output = round(prediction[0], 5)

    return render_template("index.html", \
        prediction_text="Used Car's Price prediction $ {}".format(output))
    
if __name__ == "__main__":
    app.run(debug=True)

