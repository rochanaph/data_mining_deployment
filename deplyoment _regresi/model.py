import pandas as pd
import os
import sys
import pickle
sys.stderr = open(os.devnull, "w")
from sklearn import ensemble
sys.stderr = sys.__stderr__
path = "D:/Main/Kuliah TRPL/Prak Data Mining/Pertemuan 5 Deployment Regresi/deplyoment"

# load data train untuk pemodelan
df = pd.read_csv(path+"/Data_Train.csv")
dfBaru = pd.DataFrame()
dfBaru['Seats'] = df['Seats']

# preprocessing1 ubah string ke numerik
def splitSatuan(text):
    '''
    fungsi menerima input berupa string. men-split string berdasar spasi.
    mengambil element pertama hasil split. yakni string yg berupa angka.
    return: float angka
    '''
    hasilSplit = text.split() #split berdasar spasi
    angka = hasilSplit[0] # akses elemen pertama
    angka = float(angka) # convert tipe string mjd float
    return angka
print("============Convert str to numeric..=========")
df = df.astype({'Mileage':str, 'Engine':str})
dfBaru['Mileage'] = df['Mileage'].apply(splitSatuan)
dfBaru['Engine'] = df['Engine'].apply(splitSatuan)

# preprocessing2 replace missing value
print("============Replace missing value...=========")
dfBaru['Mileage'].fillna(dfBaru['Mileage'].mean(), inplace=True)
dfBaru['Engine'].fillna(dfBaru['Engine'].mean(), inplace=True)
dfBaru['Seats'].fillna(dfBaru['Seats'].mode()[0], inplace=True)

# pemodelan regresi
print("============Modeling................=========")
modelRegresi = ensemble.GradientBoostingRegressor()
fitur = dfBaru
target = df['Price']
modelRegresi.fit(fitur, target)

# menyimpan model ke pickle
print("============Saving model............=========")
pickle.dump(modelRegresi, open(path+"/model.pkl","wb"))
